# USAGE
# python opa.py image_name.format

# import the necessary packages
import sys
import numpy as np
import imutils
import cv2
import json
import mysql.connector

def draw_rectangle(event, x, y, flags, param):
    global mouseX1, mouseY1, mouseX2, mouseY2
    # Check mouse click event
    if event == cv2.EVENT_LBUTTONDOWN:
        # Draw rectangle in the clicked region
        #cv2.rectangle(img_full, (x-50, y-50), (x+50, y+50), (255, 0, 0), 1)
        # Store clicked region in global variables
        mouseX1, mouseY1, mouseX2, mouseY2 = x-50, y-50, x+50, y+50

        # Prevent negative locations
        if(mouseX1 < 0):
            mouseX1 = 1
        if(mouseY1 < 0):
            mouseY1 = 1
        # Call object detetion
        object_detect()


def object_detect():
    img = img_full[mouseY1:mouseY2, mouseX1:mouseX2]
    #crop_img = img[y:y+h, x:x+w].copy()
    #cv2.imshow("cropped", img)
    # cv2.waitKey(0)
    img = imutils.resize(img, width=np.size(img, 1))

    # grab the image dimensions and convert it to a blob
    (h, w) = img.shape[:2]
    blob = cv2.dnn.blobFromImage(cv2.resize(img, (300, 300)),
                                 0.007843, (300, 300), 127.5)

    # pass the blob through the network and obtain the detections and
    # predictions
    net.setInput(blob)
    detections = net.forward()
    detection_count=0
    # loop over the detections
    for i in np.arange(0, detections.shape[2]):
        # extract the confidence (i.e., probability) associated with
        # the prediction
        confidence = detections[0, 0, i, 2]
        # filter out weak detections by ensuring the `confidence` is
        # greater than the minimum confidence
        if confidence > 0.2:
            # extract the index of the class label from the
            # `detections`, then compute the (x, y)-coordinates of
            # the bounding box for the object
            idx = int(detections[0, 0, i, 1])
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")

            if CLASSES[idx] in IGNORE:
                continue

            MATCH.append(CLASSES[idx])
            # draw the prediction on the img
            '''label = "{}: {:.2f}%".format(CLASSES[idx],
                                         confidence * 100)
            cv2.rectangle(img, (startX, startY), (endX, endY),
                          COLORS[idx], 2)
            y = startY - 15 if startY - 15 > 15 else startY + 15
            cv2.putText(img, label, (startX, y),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, COLORS[idx], 2)'''
            # print the extracted data
            print("{} {:.2f}%".format(CLASSES[idx], confidence * 100))
            detection_count+=1
            if(detection_count==1):
            	break

    # show the output img
    #cv2.imshow("img", img)
    # cv2.waitKey(0)


# initialize the list of class labels MobileNet SSD was trained to
# detect, then generate a set of bounding box colors for each class
CLASSES = ["background", "aeroplane", "bicycle", "bird", "boat",
           "bottle", "bus", "car", "cat", "chair", "cow", "diningtable",
           "dog", "horse", "motorbike", "person", "pottedplant", "sheep",
           "sofa", "train", "tvmonitor"]

# initialize the list of lables to ignore from detection
IGNORE = ["background", "tvmonitor"]

#PASSWORD = ["cow", "boat", "cow", "aeroplane"]
MATCH = []

db = mysql.connector.connect(
  host="localhost",
  user="root",
  passwd="root",
  database="object_authentication"
)

cursor = db.cursor()

# Load user details from database, username received as command line argument
cursor.execute("SELECT * FROM users WHERE username = %s",(sys.argv[2],))

result = cursor.fetchone()

# Exit program if username doesn't exist
if(cursor.rowcount==-1):
	print("Username not found!")
	exit()

# Convert json password text to list
PASSWORD = json.loads(result[2])

COLORS = np.random.uniform(0, 255, size=(len(CLASSES), 3))

# load our serialized model from disk
print("[INFO] loading model...")
net = cv2.dnn.readNetFromCaffe("MNS/MNS.txt", "MNS/MNS.caffemodel")

print("[INFO] starting object detection...")
# grab the image resize it
# to have a maximum width of 1000 pixels
img_full = cv2.imread(sys.argv[1])

cv2.namedWindow('image')
cv2.setMouseCallback('image', draw_rectangle)
while(1):
    cv2.imshow('image', img_full)
    k = cv2.waitKey(20) & 0xFF
    # if k == 32:
    #	object_detect()
    if k == 27:
        break
    elif k == ord('a'):
        print (mouseX1, mouseY1)

# Match user password and entered password
auth=True
if len(PASSWORD)==len(MATCH):
	for i in range(len(PASSWORD)):
		if(PASSWORD[i]!=MATCH[i]):
			auth = False
			break
else:
	auth = False
if(auth):
	print("Success")
else:
	print("Invalid")
