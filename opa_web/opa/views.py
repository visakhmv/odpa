from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse


def index(request):
	return render(request, 'login.html')
	
def register(request):
	return render(request, 'register.html')
	
def user_verify(request):
	return HttpResponse("pass1.png")
	
def user_authenticate(request):
	import json
	# import object detection function
	from .detect import object_detect
	# import models
	from opa.models import User
	
	# parse json data from login page
	data=json.loads(request.body)
	#print(request.body)
	try:
		# load user details from database
		r1 = User.objects.get(username=data["user"])
		#print(r1.password)
		# convert json password to list
		PASSWORD = json.loads(r1.password)
	except User.DoesNotExist:
		return HttpResponse("Invalid")
	if(PASSWORD!=None):
		# detect objects from image and coordinates
		OBJECTS = object_detect(data["img"], data["pass"])
		return HttpResponse("Success") if (PASSWORD==OBJECTS) else HttpResponse("Invalid")
		
