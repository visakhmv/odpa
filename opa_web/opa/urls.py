from django.urls import path

from . import views, user

urlpatterns = [
    path('', views.index, name='index'),
    path('register', views.register, name='register'),
    path('user_verify', views.user_verify, name='user_verify'),
    path('user_authenticate', views.user_authenticate, name='user_authenticate'),
    path('user', user.index, name='user'),
]
